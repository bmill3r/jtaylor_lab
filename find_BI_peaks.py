#!/usr/bin/env python
#(c) 2013 Emory University. All Rights Reserved
# Code written by: Michael Sauria (mgehrin@emory.edu)

import sys

import numpy
from pyx import *

import hifive


unit.set(defaultunit="cm")
text.set(mode="latex")
text.preamble(r"\usepackage{times}")
text.preamble(r"\usepackage{sansmath}")
text.preamble(r"\sansmath")
text.preamble(r"\renewcommand*\familydefault{\sfdefault}")
painter = graph.axis.painter.regular(labelattrs=[text.size(-3)], titleattrs=[text.size(-2)], basepathattrs=[color.cmyk.White])


def main():
    hic_fname, BI_fname, out_fname, cutoff = sys.argv[1:5]
    cutoff = float(cutoff)
    if len(sys.argv) > 5:
        chroms = sys.argv[5].split(',')
    else:
        chroms = []
    BI = hifive.bi.BI()
    BI.load(BI_fname)
    peaks = BI.find_bi_bounds(cutoff=cutoff, chroms=chroms)
    print >> sys.stderr, ("Total peaks: %i\n") % (peaks.shape[0]),
    write_peaks(BI, peaks, out_fname)
    max_distance = 1000000
    binsize = 25000
    for chrom in chroms: #for chrom in ['19']:#chroms:
        plot_data(chrom, hic_fname, BI, peaks, max_distance, binsize, "%s_%s.pdf" % (out_fname, chrom))


def write_peaks(BI, peaks, fname):
    output = open(fname, 'w')
    chroms = BI.chromosomes
    for i in range(peaks.shape[0]):
        print >> output, "%s\t%i\t%i\tpeak_%05i\t%f\t+" % (chroms[peaks['chr'][i]], peaks['coord'][i],
                                                           peaks['coord'][i], i, peaks['score'][i])
    output.close()
    return None


def plot_data(chrom, hic_fname, BI, peaks, max_distance, binsize, out_fname):
    hic = hifive.analysis.HiC(hic_fname, 'r')
    scores = BI.BI[numpy.where(BI.BI['chromosome'] == BI.chr2int[chrom])[0]]
    smoothed = numpy.copy(scores['score'])
    min_score = numpy.amin(smoothed)
    max_score = numpy.amax(smoothed)
    chr_peaks = peaks['coord'][numpy.where(peaks['chr'] == BI.chr2int[chrom])[0]].astype(numpy.float32)
    width = 10.0
    array, mapping = hifive.hic.binning.bin_cis_signal(hic, chrom, binsize=binsize,
                                             maxdistance=maxdistance, arraytype='compact', returnmapping=True)
    min_X = mapping[0, 2] + binsize / 2
    max_X = mapping[-1, 3] - binsize / 2
    hm_width = (mapping[-1, 3] - mapping[0, 2]) / 1000000.0 * width
    plot_width = (mapping[-1, 3] - mapping[0, 2] - binsize) / 1000000.0 * width
    c = canvas.canvas()
    c.insert(bitmap.bitmap(0, 0, hifive.plotting.plot_diagonal_from_compact_array(array), width=hm_width),
        [trafo.translate((plot_width - hm_width) / 2, 0)])
    g = graph.graphxy(width=plot_width, height=1.0, 
    x=graph.axis.lin(min=min_X, max=max_X, title='', painter=painter), 
    y=graph.axis.lin(min=min_score, max=max_score, title='', parter=None), 
    x2=graph.axis.lin(min=0, max=1, parter=None),
    y2=graph.axis.lin(min=0, max=1, parter=None) )
    chr_peaks -= min_X
    chr_peaks /= (max_X - min_X)
    chr_peaks *= plot_width
    where = numpy.where((chr_peaks >= 0) * (chr_peaks < plot_width))[0]
    for i in where:
        c.stroke(path.line(chr_peaks[i], -1.0, chr_peaks[i], 0),
                 [color.rgb.red, style.linewidth.Thin])
    where = numpy.where((scores['mid'] >= min_X) * (scores['mid'] < max_X))[0]
    g.plot(graph.data.values(x=scores['mid'][where], y=smoothed[where]),
           [graph.style.line([color.rgb.red, style.linewidth.THin])])
    c.insert(g, [trafo.translate(0, -1.0)])
    c.writePDFfile(out_fname)
    return None


if __name__ == '__main__':
    main()
