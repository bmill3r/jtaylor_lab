From 09_17_14:
Questions still need to be answered:

Hey Michael,

A few questions about the 'find_hic_BI.py' script:

1. What does the height input metric represent?
2. What about the smoothing metric? In the script I see it appear as input for the BI.smooth_bi(), however in the bi.py module the input for BI.smooth_bi is actually width..

3. For BI.find_bi_from_hic(), what does datatype='enrichment' do? In the bi.py module, datatype='fend'. Not sure where to find descriptions for other datatypes that can be used.


I assume that 'find_hic_BI.py' will return a hdf5 file containing Boundary Indexes, which I can use in the 'find_BI_peaks.py' script you sent me to call bounds and plot them.


Thanks,
Brendan

ANSWERS:
Sorry this took so long to return.

1. The width and height together determine the size of the bins being used (take a look at the figure from the paper describing the BI).

2. As for smoothing, the BI uses a normal-shaped curve to weight values surrounding the target value. The standard deviation of this normal curve is given by the smoothing parameter. I've included a figure from my dissertation that graphically shows how the smoothing works.
 
3. datatype refers to the type of correction that is applied before calculating the BI. The options are 'raw', 'fend', 'distance', and 'enrichment'. Raw applies no correction to the data, fend only corrects for the individual fend biases, distance removes the distance-dependent portion of the signal, and enrichment removes both the distance and fend bias parts of the signal.

And yes, find_hic_BI will return a BI.hdf5 object for the find_BI_peaks.py script.

---------------------------------------------------------------------------------------------

1. Write PBS script to submit with qsub on HHPC cluster (use nano)


#!/bin/bash

#PBS -V
#PBS -N BI_output
#PBS -l nodes=40:ppn=2

INPUT=/home/bmiller/GSE35156_hESC_hic.hdf5
OUTPUT=/home/bmiller/hESC_BI_out.hdf5
WIDTH=100000
WINDOW=500000
HEIGHT=100000
MINCOUNT=10
SMOOTHING=5000
#CHROMS=

#mpirun -np python /home/bmiller/find_hic_BI.py $INPUT $OUTPUT $WIDTH $WINDOW $HEIGHT $MINCOUNT $SMOOTHING

python /home/bmiller/find_hic_BI.py $INPUT $OUTPUT $WIDTH $WINDOW $HEIGHT $MINCOUNT $SMOOTHING

_________________________________________________________________________________________________


Issue:
	hifive not recognized as module...need to install it in home directory

	Copied hifive folder (setup.py and lib) to a new hifive folder in home directory on cluster
	'python26 setup.py install' almost works, but permission denied when making site-packages directory...
	Don't have permission to sudo either...

		(Can't sudo on cluster...)

UPDATE:
	wget https://bitbucket.org/bxlab/hifive/get/b8e96d9e1f89.zip
		(downloaded hifive repository from bitbucket)

	moved into new directory hifive_src

	[bmiller@login hifive_src]$ unzip b8e96d9e1f89.zip
		(unzipped file)

		cd bxlab-hifive-b8e96d9e1f89
			(moved into directory, has setup.py and packages...)

	[bmiller@login bxlab-hifive-b8e96d9e1f89]$ python2.6 setup.py install --user
		(use python2.6 b/c default on cluster is 2.4, which doesn't have cython.distutils...)

		--user              install in user site-package
                      '/home/bmiller/.local/lib/python2.6/site-packages'


	now python26 import hifive works! However...new error: no module h5py...need to install...

"""
	We have switched development to GitHub.  Here's how to build
h5py from source:

1. Clone the project::
   
      git clone https://github.com/h5py/h5py.git

2. Build the project (this step also auto-compiles the .c files)::
  
      python setup.py build [--hdf5=/path/to/hdf5]

3. Run the unit tests (optional)::
  
      python setup.py test

"""

downloaded from github
copied /h5py into .local/lib/python2.6/site-packages/
python2.6 setup.py install --user

python26
>>>import h5py
error: import h5py
	cannot import _error

Not sure what to do...tried build and other things but not working.
Interestingly, after leaving directory with h5py...import in python2.6 says it can't find h5py...


TESTING SCRIPT find_hic_BI.py on home computer:

/Users/cmdb/Desktop $ python find_hic_BI.py GSE35156/GSE35156_hESC_hic.hdf5 BI_out_hESC.hdf5 100000 500000 100000 10 5000

Could not find GSE35156_hESC_data.hdf5. No data loaded.
Could not find HG19_HindIII.hdf5. No fends loaded.
Traceback (most recent call last):
  File "find_hic_BI.py", line 42, in <module>
    find_BI(hic_fname, BI_fname, width, window, height, mincount, smoothing, chroms)
  File "find_hic_BI.py", line 25, in find_BI
    hic = hifive.analysis.HiC(hic_fname, 'r')
  File "/Library/Python/2.7/site-packages/hifive/analysis.py", line 84, in __init__
    self.load()
  File "/Library/Python/2.7/site-packages/hifive/analysis.py", line 180, in load
    for i, chrom in enumerate(self.fends['chromosomes']):
AttributeError: 'HiC' object has no attribute 'fends'


When hESC_hic.hdf5 is loaded as a HiC class object in this script, I don't believe there is a 'fends' key in the hdf5 file. Looking around line 180 in analysis.py, in the def load(self) attribute, I see where the errors: "Could not find GSE35156_hESC_data.hdf5. No data loaded." and "Could not find HG19_HindIII.hdf5. No fends loaded." are called.

So I don't think there is fend data in the *_hic.hdf5 files. This might make sense because they are post-analysis data.

However, I tried again with one of the *_data.hdf5 files, which should contain fend data, and got a very similar error:

/Users/cmdb/Desktop $ python find_hic_BI.py GSE35156_hESC_data.hdf5 BI_out_hESC.hdf5 100000 500000 100000 10 5000

Could not find HG19_HindIII.hdf5. No fends loaded.
Traceback (most recent call last):
  File "find_hic_BI.py", line 42, in <module>
    find_BI(hic_fname, BI_fname, width, window, height, mincount, smoothing, chroms)
  File "find_hic_BI.py", line 25, in find_BI
    hic = hifive.analysis.HiC(hic_fname, 'r')
  File "/Library/Python/2.7/site-packages/hifive/analysis.py", line 84, in __init__
    self.load()
  File "/Library/Python/2.7/site-packages/hifive/analysis.py", line 180, in load
    for i, chrom in enumerate(self.fends['chromosomes']):
AttributeError: 'HiC' object has no attribute 'fends'


Not sure where this "HG19_HindIII.hdf5 file is or how to load it into the script...
	

	



