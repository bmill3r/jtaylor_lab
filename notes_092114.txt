Generated BIs for samples...
hESC has insufficient data for Y chromosome
hIMR90 has insufficient data for Y chromosome

Mice have different chromosomes...
mCortex has chromosomes 1-19, X and Y insufficient data
mESC_HindIII has chroms1-19 and X and Y are good
mESC_NcoI has chroms 1-19 and X and Y are good


Used same options (width, height, smoothing, etc) for all

EX:
/Users/cmdb/Desktop/GSE35156 $ python find_hic_BI.py HiFive/GSE35156_hESC_hic.hdf5 BI_out_hESC.hdf5 100000 500000 100000 10 5000 '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,21,X'

Calculating BI... Finding enrichment compact array for 1:18149-249231261... Done
Finding enrichment compact array for 2:15486-243186785... Done
Finding enrichment compact array for 3:62259-197961569... Done
Finding enrichment compact array for 4:21051-191033558... Done
Finding enrichment compact array for 5:17664-180899443... Done
Finding enrichment compact array for 6:67365-171054006... Done
Finding enrichment compact array for 7:10885-159125134... Done
Finding enrichment compact array for 8:14869-146301937... Done
Finding enrichment compact array for 9:18262-141151803... Done
Finding enrichment compact array for 10:73875-135516047... Done
Finding enrichment compact array for 11:61233-134943202... Done
Finding enrichment compact array for 12:65000-133837825... Done
Finding enrichment compact array for 13:19022703-115109283... Done
Finding enrichment compact array for 14:19001627-107289162... Done
Finding enrichment compact array for 15:20001782-102513012... Done
Finding enrichment compact array for 16:66762-90292810... Done
Finding enrichment compact array for 17:936-81188173... Done
Finding enrichment compact array for 18:28269-78009977... Done
Finding enrichment compact array for 19:67032-59110814... Done
Finding enrichment compact array for 20:60682-62963310... Done
Finding enrichment compact array for 21:9412792-48111151... Done
Finding enrichment compact array for 21:9412792-48111151... Done
Finding enrichment compact array for X:61550-155254048... Done
Done
Finding smoothed BIs... Done
Saving data... Done



NEXT STEPS:
1. Combine BIs from mESC HindIII and NcoI..
2. Plot BIs
3. Find regions that do have conserved TADs and those that do not
4. Of the TADs that are not conserved, are these located in regions of genome that are similar for mouse and human? Use UCSC genome browser to see...

Probably best to compare mouse ESC and human ESC first.

Chroms 1-19 in mice same as chroms 1-19 in humans? What about 20, 21 and 22?

What kinds of cells are these data sets made up of?
And how should these cell types be compared to one another?

Different chromosomes cause and issue for comparison? Throw out ones that aren't consistent through each data set?

Adjusting options may give data for chromosomes with initially 'insufficient data'? What do these options do exactly?? How do they affect data output? If they are changed can data still be compared between files?


