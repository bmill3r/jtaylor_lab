1. Changed intersecting_genes.py
	so that gene coordinates that overlap with features on ends also included.

	This probably important with peak_intervals because they are 20kb or less.


2. bn-mapper.py
	used to generate peak_intervals that are either mapped
	or not mapped...

----------------------------------------------------------
bnMapper.py from_features.bed alignment.chain -o to_features.bed

hESC:
bnMapper.py ../analysis/hESC/peak_intervals/hESC_peak_intervals_bed4_filtered-20kb.txt ../genome_chain_files/hg19ToMm9.over.chain | grep 'INFO:' -v > hESC_peak_intervals_20kb_bn-mapped-output.txt

mESC_HindIII:
bnMapper.py ../analysis/mESC_HindIII/peaks_intervals/mESC_HindIII_peak_intervals_bed4_filtered-20kb.txt ../genome_chain_files/mm9ToHg19.over.chain | grep 'INFO:' -v > mESC_HindIII_peak_intervals_20kb_bn-mapped-output.txt


These output files have coordinates of query sequence for which features of target sequence mapped (last column has ID of each mapped target feature)


NEXT: Use this information to generate BED4 files that have the bn-mapped and unmapped peaks...

	1. awk '{print $NF}' mESC_HindIII_peak_intervals_20kb_bn-mapped-output.txt | sort -u -n > mESC_HindIII_peak_intervals_20kb_bn-mapped-list.txt

	   awk '{print $NF}' hESC_peak_intervals_20kb_bn-mapped-output.txt | sort -u -n > hESC_peak_intervals_20kb_bn-mapped-list.txt


	2. Use lists of mapped peaks to make BED4 files:

		BED4-from-bnMap-list.py

		./BED4-from-bnMap-list.py hESC_peak_intervals_20kb_bn-mapped-list.txt ../analysis/hESC/peak_intervals/hESC_peak_intervals_bed4_filtered-20kb.txt hESC_peak_intervals_20kb-bn

		./BED4-from-bnMap-list.py mESC_HindIII_peak_intervals_20kb_bn-mapped-list.txt ../analysis/mESC_HindIII/peaks_intervals/mESC_HindIII_peak_intervals_bed4_filtered-20kb.txt mESC_HindIII_peak_intervals_20kb-bn



	Use these in intersecting_genes.py to get new gene lists....

hESC:
./intersecting_genes.py ../UCSC_tables/genes/humanGenes_hg19-mm-orths_geneSymb.txt ../scripts/hESC_peak_intervals_20kb-bn-mapped.txt hESC_peak_intervals_20kb-bn-mapped_intersectGenes_symb.txt

./intersecting_genes.py ../UCSC_tables/genes/humanGenes_hg19-mm-orths_geneSymb.txt ../scripts/hESC_peak_intervals_20kb-bn-unmapped.txt hESC_peak_intervals_20kb-bn-unmapped_intersectGenes_symb.txt


mESC_HindIII:
./intersecting_genes.py ../UCSC_tables/genes/mouseGenes_mm9-hg-orths_geneSymb.txt ../scripts/mESC_HindIII_peak_intervals_20kb-bn-mapped.txt mESC_HindIII_peak_intervals_20kb-bn-mapped_intersectGenes_symb.txt

./intersecting_genes.py ../UCSC_tables/genes/mouseGenes_mm9-hg-orths_geneSymb.txt ../scripts/mESC_HindIII_peak_intervals_20kb-bn-unmapped.txt mESC_HindIII_peak_intervals_20kb-bn-unmapped_intersectGenes_symb.txt
------------------------------------------------------------
intersecting_genes.py

Rerun with changes, for TADs and peak_intevals (after using ones with bn-mapper.py)


TADs:

hESC:
./intersecting_genes.py ../UCSC_tables/genes/humanGenes_hg19-mm-orths_geneSymb.txt ../analysis/hESC/TADs/hESC_TAD_bed4_1Mb_lift-mapped.txt hESC_TAD_1Mb-lift-mapped_intersectGenes_symb.txt

./intersecting_genes.py ../UCSC_tables/genes/humanGenes_hg19-mm-orths_geneSymb.txt ../analysis/hESC/TADs/hESC_TAD_bed4_1Mb_lift-unmapped.txt hESC_TAD_1Mb-lift-unmapped_intersectGenes_symb.txt
	
mESC_HindIII:
./intersecting_genes.py ../UCSC_tables/genes/mouseGenes_mm9-hg-orths_geneSymb.txt ../analysis/mESC_HindIII/TADs/mESC_HindIII_TAD_bed4_1Mb_lift-mapped.txt mESC_HindIII_TAD_1Mb_lift-mapped_intersectGenes_symb.txt

./intersecting_genes.py ../UCSC_tables/genes/mouseGenes_mm9-hg-orths_geneSymb.txt ../analysis/mESC_HindIII/TADs/mESC_HindIII_TAD_bed4_1Mb_lift-unmapped.txt mESC_HindIII_TAD_1Mb_lift-unmapped_intersectGenes_symb.txt


These lists much larger, especially after allowing genes that overlap, but aren't necessarily confined to the TAD region

Do geneFeatureAnalysis.py again....

--------------------------------------------------------------------

REDO geneFeatureAnalysis for TADs and peak_intervals:

Lists over overlapping genes not confined to genes that fall only within features.
peak_intervals now mapped via bnMapper and thus approx 70% mapped between human and mouse
	(same with TADs, which were mapped via lift-over)


hESC, TADs:
./geneFeatureAnalysis.py ../analysis/hESC/TADs/hESC_TAD_bed4_1Mb_lift-mapped.txt ../analysis/hESC/TADs/hESC_TAD_1Mb-lift-mapped_intersectGenes_symb.txt ../UCSC_tables/genes/humanGenes_hg19-mm-orths_geneSymb.txt 
3 features contain 0 genes
hESC_TAD_1Mb-lift-mapped-intersect_...png

./geneFeatureAnalysis.py ../analysis/hESC/TADs/hESC_TAD_bed4_1Mb_lift-unmapped.txt ../analysis/hESC/TADs/hESC_TAD_1Mb-lift-unmapped_intersectGenes_symb.txt ../UCSC_tables/genes/humanGenes_hg19-mm-orths_geneSymb.txt 
3 features contain 0 genes
hESC_TAD_1Mb-lift-unmapped-intersect_...png


hESC, peak_intervals:
./geneFeatureAnalysis.py ../analysis/hESC/peak_intervals/hESC_peak_intervals_20kb-bn-mapped.txt ../analysis/hESC/peak_intervals/hESC_peak_intervals_20kb-bn-mapped_intersectGenes_symb.txt ../UCSC_tables/genes/humanGenes_hg19-mm-orths_geneSymb.txt 
47 features contain 0 genes
hESC_peak_intervals_20kb-bn-mapped-intersect_...png

./geneFeatureAnalysis.py ../analysis/hESC/peak_intervals/hESC_peak_intervals_20kb-bn-unmapped.txt ../analysis/hESC/peak_intervals/hESC_peak_intervals_20kb-bn-unmapped_intersectGenes_symb.txt ../UCSC_tables/genes/humanGenes_hg19-mm-orths_geneSymb.txt 
21 features contain 0 genes
hESC_peak_intervals_20kb-bn-unmapped-intersect_...png


mESC_HIndIII, TADs:
./geneFeatureAnalysis.py ../analysis/mESC_HindIII/TADs/mESC_HindIII_TAD_bed4_1Mb_lift-mapped.txt ../analysis/mESC_HindIII/TADs/mESC_HindIII_TAD_1Mb-lift-mapped_intersectGenes_symb.txt ../UCSC_tables/genes/mouseGenes_mm9-hg-orths_geneSymb.txt
3 features contain 0 genes
mESC_HindIII_TAD_1Mb-lift-mapped_intersect_...png

./geneFeatureAnalysis.py ../analysis/mESC_HindIII/TADs/mESC_HindIII_TAD_bed4_1Mb_lift-unmapped.txt ../analysis/mESC_HindIII/TADs/mESC_HindIII_TAD_1Mb-lift-unmapped_intersectGenes_symb.txt ../UCSC_tables/genes/mouseGenes_mm9-hg-orths_geneSymb.txt 
2 features contain 0 genes
mESC_HindIII_TAD_1Mb-lift-unmapped_intersect_...png


mESC_HindIII, peaks_intervals:
./geneFeatureAnalysis.py ../analysis/mESC_HindIII/peaks_intervals/mESC_HindIII_peak_intervals_20kb_bn-mapped.txt ../analysis/mESC_HindIII/peaks_intervals/mESC_HindIII_peak_intervals_20kb-bn-mapped_intersectGenes_symb.txt ../UCSC_tables/genes/mouseGenes_mm9-hg-orths_geneSymb.txt 
30 features contain 0 genes
mESC_HindIII_peak_intervals_20kb_bn-mapped_intersect_...png

./geneFeatureAnalysis.py ../analysis/mESC_HindIII/peaks_intervals/mESC_HindIII_peak_intervals_20kb_bn-unmapped.txt ../analysis/mESC_HindIII/peaks_intervals/mESC_HindIII_peak_intervals_20kb-bn-unmapped_intersectGenes_symb.txt ../UCSC_tables/genes/mouseGenes_mm9-hg-orths_geneSymb.txt 
12 features contain 0 genes
mESC_HindIII_peak_intervals_20kb_bn-unmapped_intersect_...png



GENES ARE HIGHLY ENRICHED ON 3' ENDS...and not 5'...could this be due to how gene coordinates are organized? And +/- strand not taken into account...?? Seems that the coordinates given for each gene do correspond the the interval for which the gene occupies...although the direction for which it is transcribed varies with + or -..

Ran geneFeatureAnalysis on hESC_TAD_lift-mappped and only used transcripts from + strand:
	same pattern....

====================================================================================
NEXT:

Is it possible to find mapped features in human and mouse that correspond to one another...
For example..mapped coordinates of a human feature correspond to a mouse feature within those same coordinates?

corresponding_features.py

		problem with TADs: coordinates to query genome for a given feature are spread over multiple chromosomes..unlike bnMapper. Maybe need to refine liftOver filters..or find different program?



NEXT:
What types of genes are transcribed in TADs vs PEAKS, and for CONSERVED vs NOT CONSERVED?


