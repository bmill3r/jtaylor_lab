#!/usr/bin/env python

import hifive
import numpy
import pandas
import sys

#chromos is string in which each chromosome seprated by a comma: 1,2,3,4,5....,
BI_file_path, path_to_peak_file, BI_bedGraph_output, peak_interval_output, chromos = sys.argv[1:6]

#load the BI.hdf5 file into a BI object
BI = hifive.bi.BI()
BI.load(BI_file_path)

BI_bedGraph = open(BI_bedGraph_output, 'w')
peak_intervals = open(peak_interval_output, 'w')

chromos = chromos.split(',')

bedGraph_list = []
#loop through each chromosome in chromos
for i in chromos:
    #generate array of information for each BI_inveral for a given chromosome
    scores = BI.BI[numpy.where(BI.BI['chromosome'] == BI.chr2int[i])]
    #for each interval in array, append a line with certain information to BI_bedGraph file
    for line in scores:
        chrom, start, stop, mid, score, original = line
        #get information into a list to append into bedGraph_list
        trueChrom = 'chr'+i
        positions = [trueChrom, start, stop, mid]
        #get a string entry ready to write to bedGraph output file
        entry = 'chr'+str(i)+'\t'+str(start)+"\t"+str(stop)+"\t"+str(score)+"\n"
        BI_bedGraph.write(entry)
        #use bedGraph_list (a list of lists) to generate array to be used to get peak_intervals
        bedGraph_list.append(positions)


df_bi = pandas.DataFrame(bedGraph_list)
df_peaks = pandas.read_table(path_to_peak_file, header=None)
#Make arrays of BI_bedGraph file contents and peak_file contents
array_bi = numpy.array(df_bi)
array_peaks = numpy.array(df_peaks)

#loop through each line in array of peaks
#goal is to match the peak coord (midpoint of a BI) to the proper BI
for i in array_peaks:
    chrm = 'chr'+str(i[0])
    coord = i[1]
    if chrm in array_bi[:,0]:
        #make a temp array of BI intervals that are in the same chromosome as the given entry in array_peaks
        temp_BI_array = array_bi[array_bi[:,0] == chrm]
    else:
        continue
    for j in temp_BI_array:
        #if peak_coord is within an interval of a BI, that BI must coorespond to that peak
        if j[1] < coord < j[2]:
            #write a line containing chr#, start, stop to peak_interval file
            #Could add BI midpoint values with j[3]...useful to better match peaks to BI_intervals?
            entry = chrm+'\t'+str(j[1])+'\t'+str(j[2])+'\n'
            peak_intervals.write(entry)
        else:
            continue


BI_bedGraph.close()
peak_intervals.close()
            
            
            