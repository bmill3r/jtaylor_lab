Downloaded files from cluster:
	Bing Ren cells from human and mouse
	Stored in ~/Desktop/GSE35156

*_hic.hdf5 files have been processed by HiFive...ready for boundary calls...

On cluster:
/data1/gpumaster/taylor/users/mgehrin/projects/hic_contacts/Data/HiC/HiFive

-rw-rw-r--+ 1 james bxlab 13483344 Jun 29 12:02 GSE34587_mCortex_hic.hdf5
-rw-rw-r--+ 1 james bxlab 13412288 Jun 29 12:02 GSE35156_hESC_hic.hdf5
-rw-rw-r--+ 1 james bxlab 13412288 Jun 29 12:02 GSE35156_hIMR90_hic.hdf5
-rw-rw-r--+ 1 james bxlab 13483344 Aug 18 14:05 GSE35156_mCortex_hic.hdf5
-rw-rw-r--+ 1 james bxlab 13483344 Jun 29 12:02 GSE35156_mESC_HindIII_hic.hdf5
-rw-rw-r--+ 1 james bxlab 13654448 Jun 29 12:02 GSE35156_mESC_NcoI_hic.hdf5
-rw-rw-r--+ 1 james bxlab 13487440 Aug 15 15:03 GSE40173_mPreProB_hic.hdf5
-rw-rw-r--+ 1 james bxlab 13487440 Aug 15 15:03 GSE40173_mProB_hic.hdf5
-rw-rw-r--+ 1 james bxlab 13412288 Jun 29 12:02 GSE43070_hESC_H1_hic.hdf5
-rw-rw-r--+ 1 james bxlab 13412288 Jun 29 12:02 GSE43070_hIMR90_hic.hdf5
-rw-rw-r--+ 1 james bxlab 13412288 Jun 29 12:02 GSE48592_hGM12878_hic.hdf5
-rw-rw-r--+ 1 james bxlab 13483344 Aug 15 11:34 GSE48592_mESC_F123_hic.hdf5

scp bmiller@login.hhpc.jhu.edu:.../hic_contacts/Data/HiC/HiFive/GSE35156* ~/Desktop/GSE35156



Read Michael's Dissertation 



NEXT STEPS:
1. Continue to read through scripts and understand workflow of HiFive..

2. Use find_bi_peaks.py to get BI values in output file
3. Visualize BI values and plot...write script using some of Michael's suggestions.
	Questions for Michael about some of the inputs for functions

4. Find non-homologous boundaries between different samples.... 