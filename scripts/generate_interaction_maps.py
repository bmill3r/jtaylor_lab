#!/usr/bin/env python

import hifive
from pyx import *
import sys

'''
Can generate individual interaction plots for a given region!
Use 25000 binsize, seems to give good resultion, but play around...
Invert image in ppt

./test.py /Users/cmdb/Desktop/GSE35156/HiFive/GSE35156_hESC_hic.hdf5 /Users/cmdb/Desktop/GSE35156/BI/BI_out_hESC.hdf5 test 1.75 10 89000000 91000000 25000

'''

hic_fname, BI_fname, out_fname, cutoff, chrom, start, stop, binsize = sys.argv[1:9]

width = 10.0
c = canvas.canvas()


cutoff, start, stop, binsize = float(cutoff), int(start), int(stop), int(binsize)
hic = hifive.analysis.HiC(hic_fname, 'r')
array, mapping = hifive.hic.binning.bin_cis_signal(hic, chrom, start=start, stop=stop, binsize=binsize, arraytype='compact', returnmapping=True)

print mapping
min_X = mapping[0, 2] + binsize / 2
max_X = mapping[-1, 3] - binsize / 2
hm_width = (mapping[-1, 3] - mapping[0, 2]) / 1000000.0 * width
plot_width = (mapping[-1, 3] - mapping[0, 2] - binsize) / 1000000.0 * width
print min_X
print max_X
print hm_width
print plot_width

c.insert(bitmap.bitmap(0, 0, hifive.plotting.plot_diagonal_from_compact_array(array, min_color=(0.0, 0.0, 1.0), mid_color=(0.8,0.8,0.8), max_color=(1.0,0.0,0.0)), width=hm_width), [trafo.translate((plot_width - hm_width) / 2, 0)])

c.writePDFfile(out_fname)
