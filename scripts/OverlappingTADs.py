#!/usr/bin/env python

import pandas
import numpy
import sys

'''
species1_tads:
    BED4 file of TADs in species 1
    
        (Typically, use TADs that were able to be mapped to species 2)
    
species2_tad_mapped:
    BED4 file of TADs in species2 mapped to species1 (coordinates correspond to species 1)
    
    
Output BED4 file of TADs in species 1 that have intersections with the mapped TADs from species 2

And Output BED4 file of TADs in species 1 that do not intersect well with mapped TADs from species 2



        intersection = TAD coordinates fall within a species 1 TAD at least more than 20%
'''

species1_tads, species2_tads_mapped, output_file = sys.argv[1:4]
species1_tads = pandas.read_table(species1_tads, header=None)
species2_tads_mapped = pandas.read_table(species2_tads_mapped, header=None)

#Make sure all species 2 mapped TADs belong to chromosomes in species 1 tads
chromosomes = list(set(numpy.array(species1_tads[0]).ravel().tolist()))
species2_tads_mapped = species2_tads_mapped[species2_tads_mapped[0].isin(chromosomes)]

#Make dict where key is chromosome id and entry is array of all speies 2 mapped tads to that chromosome
species2_tads_dict = {}
for i in chromosomes:
    species2_tads_dict[i] = numpy.array(species2_tads_mapped[species2_tads_mapped[0] == i])


'''
For each species 1 TAD, see if any species 2 mapped tads intersect.

    If so, add the intersecting species 2 TADs to a list and make append spcies 1 TAD to new array

    IF not, add species 1 TAD to another array
'''

species1 = numpy.array(species1_tads)
noMatches = []
matches = []
for i in species1:
    chrom, start, stop, ID = i[0], i[1], i[2], i[3]
    length = float(stop-start)
    fraction = float(length*0.20)
    intersections = []
    for tad in species2_tads_dict[chrom]:
        tad = tad.tolist()
        if not (tad[2] <= int(float(start) + fraction) or int(float(stop) - fraction) <= tad[1]):
            intersections.append(tad[3])
    if len(intersections) == 0:
        entry = [chrom, start, stop, ID]
        noMatches.append(entry)
    if len(intersections) >= 1:
        entry = [chrom, start, stop, ID]
        #for x in intersections:
            #entry.append(str(int(x))) #Add Species 2 mapped intersecting TADs to list
        matches.append(entry)

noMatches = pandas.DataFrame(noMatches)
matches = pandas.DataFrame(matches)

noMatches.to_csv(output_file + '-NoOverlap_bed4.txt', sep='\t', header=False, index=False)
matches.to_csv(output_file + '-overlap_bed4.txt', sep='\t', header=False, index=False)










