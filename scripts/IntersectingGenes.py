#!/usr/bin/env python

import pandas
import numpy
import sys

'''
Clean up gene csv file obtained from Ensembl biomart

'human_Orthologous.txt'
human_nonOrthologous.txt'

Columns:
Ensembl Gene ID,Ensembl Transcript ID,Associated Gene Name,Chromosome Name,Gene Start (bp),Gene End (bp),Strand,Description,Homology Type

    After Cleanup:
    Associated Gene Name	Chromosome Name	Gene Start (bp)	Gene End (bp)	Strand	Description	Homology Type
    
    
    
    
Used this script with tad_files for different groups of tads...like mapped-overlap tad or no overlap tads and unmapped tads...


took number of genes that were mapped to each type of tad group and quantified with bar graphs in:
    graphMappedGenes.py
'''


gene_file, tad_file, output_file, mapped_gene_output_file = sys.argv[1:5]


'''
Clean up file
    Filter out duplicates,
    Remove Ensembl Gene and Transcript IDs
    Remove Entries with strange chromosome identification
'''
gene_df = pandas.read_csv(gene_file)
#print gene_file.head(20)
gene_df = gene_df.drop_duplicates(subset='Ensembl Gene ID')
#print gene_df.head(20)
gene_df = gene_df.iloc[:,2:]
#print gene_df.head(20)
gene_df = gene_df[gene_df['Homology Type'] == 'ortholog_one2one'] #Another Filter if needed...
chromosome_list = ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22']
gene_df = gene_df[gene_df['Chromosome Name'].isin(chromosome_list)]
#print gene_df.head(20)
print gene_df.shape

#Turn into dictionary to speed things up:
gene_dict = {}
for i in chromosome_list:
    gene_dict[i] = numpy.array(gene_df[gene_df['Chromosome Name'] == i])
#print gene_dict

'''
See if genes intersect with TADs in a given TAD file
    Position of each gene in a given TAD (??)
    Which Gene intersected which each TAD?
    Total genes that aligned to given TADs
'''
master_gene_list = []
total_count = 0
Feature_Genes = []
for tad in numpy.array(pandas.read_table(tad_file, header=None)):
    chrom, start, stop, ID = tad[0], tad[1], tad[2], tad[3]
    chrom = chrom.split('r')[1]
    intersecting_genes = []
    for gene in gene_dict[chrom]:
        gene = gene.tolist()
        if not (gene[3] <= start or stop <= gene[2]):
            total_count +=1
            intersecting_genes.append(gene[0])
            master_gene_list.append(gene[0])
    entry = [ID]
    for x in intersecting_genes:
        entry.append(x)
        
    Feature_Genes.append(entry)
    
print total_count



feature_genes = pandas.DataFrame(Feature_Genes)
feature_genes.to_csv(output_file, sep='\t', header=False, index=False)     

gene_df = gene_df[gene_df['Associated Gene Name'].isin(master_gene_list)]
gene_df.to_csv(mapped_gene_output_file, sep='\t', index=False)




