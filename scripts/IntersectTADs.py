#!/usr/bin/env python

import pandas
import numpy
import sys
from collections import defaultdict

'''
Find Intersecting mapped TADs between two different species (mouse and human foe example)



species1_TADs = bed4 file containing TADs that were mapped to species 2 
                but coordiantes are species 1 genome

species2_mapped_TADs = bed4 output from litOver with TADs from speies 2 that mapped to species 1
                        and coordinants are for species 1
                        
species1_mapped_TADs = bed4 output from litOver with TADs from speies 1 that mapped to species 2
                        and coordinants are for species 2

species2_TADs = bed4 file containing TADs that were mapped to species 1 
                but coordinants are species 2 genome
'''

species1_TADs, species2_mapped_TADs, species1_mapped_TADs, species2_TADs = sys.argev[1:5]



species1_TADs = pandas.read_table(species1_TADs, header=None)
species2_TADs_mapped = pandas.read_table(species2_mapped_TADs, header=None)

#Make dictionary where keys are chromosome IDs in species 1 TADs and entries are array of features in that chromosome
keys = species1_TADs[0]
keys = list(set(keys.tolist()))
feature_dict_spec1 = {}
for i in keys:
    #Make Arrays of features in a given chromosome to append as entries to dictionary
    feature_dict_spec1[i] = numpy.array(species1_TADs[species1_TADs[0] == i])
    #array = numpy.array(species1_TADs[species1_TADs[0] == i])
    #feature_dict[i].append(array)

#Make another dict where the keys are the feature IDs of TADs from species 1
IDs_1 = species1_TADs[3]
IDs_1 = IDs_1.tolist()
feature_IDs_spec1 = defaultdict(list)
for i in IDs_1:
    feature_IDs_spec1[i]

#If a TAD in species 2 TADs (which were mapped to species 1) intersects with a TAD of species 1, append to key in feature_ID dictionary
species2_TADs_mapped = numpy.array(species2_TADs_mapped)
for i in range(len(species2_TADs_mapped)):
    chrom, start, stop, ID = species2_TADs_mapped[i]
    if chrom in keys:
        species1_array = feature_dict_spec1[chrom]
        for j in species1_array:
            if not (stop <= j[1] or j[2] <= start):
                feature_IDs_spec1[j[3]].append(ID)
#Feature_IDs keys now IDs of features in species1 and entries are feature IDs of species2 that intersect


'''
Repeat going the otherway 
(species 1 mapped TADs and see if they intersect with TADs from species 2)
'''

species1_TADs_mapped = pandas.read_table(species1_mapped_TADs, header=None)
species2_TADs = pandas.read_table(species2_TADs, header=None)

keyS = species2_TADs[0]
keyS = list(set(keyS.tolist()))
feature_dict_spec2 = {}
for i in keyS:
    #Make Arrays of features in a given chromosome to append as entries to dictionary
    feature_dict_spec2[i] = numpy.array(species2_TADs[species2_TADs[0] == i])

#Make another dict where the keys are the feature IDs of TADs from species 1
IDs_2 = species2_TADs[3]
IDs_2 = IDs_2.tolist()
feature_IDs_spec2 = defaultdict(list)
for i in IDs_2:
    feature_IDs_spec2[i]

species1_TADs_mapped = numpy.array(species1_TADs_mapped)
for i in range(len(species1_TADs_mapped)):
    chrom, start, stop, ID = species1_TADs_mapped[i]
    if chrom in keys:
        species2_array = feature_dict_spec2[chrom]
        for j in species2_array:
            if not (stop <= j[1] or j[2] <= start):
                feature_IDs_spec2[j[3]].append(ID)


'''
Check to make sure TADs from both species map to same regions of genome
("Syntenic" TADs..)

Make new dictionaries for each species where keys are TAD IDs and 
entries are TADs that intersect from other species AND
TADs in a given entry matches to same region of genome
that key does.
'''

species1_matching_dict = defaultdict(list)
for i in IDs_1:
    species1_matching_dict[i]
    
for ID_1, tads_2 in feature_IDs_spec1.iteritems():
    for ID_2, tads_1 in feature_IDs_spec2.iteritems():
        if (ID_1 in tads_1) and (ID_2 in tads_2):
            species1_matching_dict[ID_1].append(ID_2)

            
species2_matching_dict = defaultdict(list)
for i in IDs_2:
    species2_matching_dict[i]
    
for ID_2, tads_1 in feature_IDs_spec2.iteritems():
    for ID_1, tads_2 in feature_IDs_spec1.iteritems():
        if (ID_2 in tads_2) and (ID_1 in tads_1):
            species2_matching_dict[ID_2].append(ID_1)




    
