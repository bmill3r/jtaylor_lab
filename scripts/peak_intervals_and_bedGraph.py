#!/usr/bin/env python

import hifive
import sys
import pandas
import numpy
from collections import defaultdict


BI_file_path, path_to_peaks, BI_bedGraph_output, peak_interval_output = sys.argv[1:5]


BI_bedGraph = open(BI_bedGraph_output+'.txt', 'w')
peak_intervals = open(peak_interval_output+'.txt', 'w')

BI = hifive.bi.BI()
BI.load(BI_file_path)

peaks = pandas.read_table(path_to_peaks, header=None)

chromosomes = set(numpy.array(peaks[0]).ravel().tolist())


bedGraph_list = []
#loop through each chromosome in chromos
for i in chromosomes:
    i = str(i)
    #generate array of information for each BI_inveral for a given chromosome
    scores = BI.BI[numpy.where(BI.BI['chromosome'] == BI.chr2int[i])]
    #for each interval in array, append a line with certain information to BI_bedGraph file
    for line in scores:
        chrom, start, stop, mid, score, original = line
        #get information into a list to append into bedGraph_list
        trueChrom = 'chr'+i
        positions = [trueChrom, start, stop, mid]
        #get a string entry ready to write to bedGraph output file
        entry = 'chr'+str(i)+'\t'+str(start)+"\t"+str(stop)+"\t"+str(score)+"\n"
        BI_bedGraph.write(entry)
        #use bedGraph_list (a list of lists) to generate array to be used to get peak_intervals
        bedGraph_list.append(positions)


bedGraph = pandas.DataFrame(bedGraph_list)
BI_dict = {}
for i in chromosomes:
    chrm = 'chr'+str(i)
    BI_dict[chrm] = numpy.array(bedGraph[bedGraph[0] == chrm])

peaks = numpy.array(peaks)
for i in peaks:
    chrm = 'chr'+str(i[0])
    coord = i[1]
    temp_BI_array = BI_dict[chrm]
    for BI in temp_BI_array:
        if BI[1] < coord < BI[2]:
            entry = chrm+'\t'+str(BI[1])+'\t'+str(BI[2])+'\n'
            peak_intervals.write(entry)
    





