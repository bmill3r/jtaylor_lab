#!/usr/bin/env python

import pandas
import numpy
from matplotlib import pyplot as py

hESC_total = 3492
mESC_total = 3098

minMatches = (0.05, 0.1, 0.2, 0.25, 0.3, 0.35, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.95, 1.0)

hESC_mapped = (3248, 3279, 2982, 2657, 2214, 1727, 1212, 396, 87, 24, 8, 6, 3, 0)
mESC_mapped = (2767, 2760, 2642, 2478, 2242, 1934, 1528, 610, 129, 28, 9, 4, 1, 0)

hESC_ratios = []
for i in hESC_mapped:
    value = float(i)/hESC_total
    hESC_ratios.append(value)

mESC_ratios = []
for i in mESC_mapped:
    value = float(i)/mESC_total
    mESC_ratios.append(value)


#Data for polyfit:
xValues_fit = (0.05, 0.1, 0.2, 0.25, 0.3, 0.35, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.95, 1.0, 0.05, 0.1, 0.2, 0.25, 0.3, 0.35, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.95, 1.0)
xValues_fit = numpy.array(xValues_fit)
yValues_fit = []
for i in hESC_ratios:
    yValues_fit.append(i)
for i in mESC_ratios:
    yValues_fit.append(i)
yValues = numpy.array(yValues_fit)
print xValues_fit
print yValues_fit

z = numpy.polyfit(xValues_fit, yValues_fit, 4)
p = numpy.poly1d(z)

#Data for minSize line
x = (0.35, 0.35)
y = (-0.2, 1.0)


fig = py.figure()
subplot = fig.add_subplot(1,1,1)
subplot.set_xlabel('Minimum Match Ratio')
subplot.set_ylabel('Percent of TADs Mapped')
a, = subplot.plot(minMatches, hESC_ratios, 'bo', label='hESC TADs')
b, = subplot.plot(minMatches, mESC_ratios, 'go', label='mESC TADs')
c, = subplot.plot(minMatches, p(minMatches), 'r-', label='fit')
d, = subplot.plot(x, y, 'k--')
#subplot.plot(minMatches, hESC_ratios, 'bo', label='hESC TADs', minMatches, mESC_ratios, 'go', label='mESC TADs', minMatches, p(minMatches), 'r-', label='fit', x, y, 'k--')
fig.legend([a,b,c], ['hESC TADs', 'mESC TADs', 'fit'], loc=5, shadow=True, fancybox=True)
fig.savefig('liftOver_mapped_TADs.png')
