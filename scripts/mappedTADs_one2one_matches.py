#!/usr/bin/env python

import sys
import pandas
import numpy


'''
Find intersections and mathed between TADs in species 1 and mapped TADs in species 2

species_1_tads -> TADs in species 1 that were successfully mapped with liftOver...coordinates are for species 1

species2_tads_mapped -> species 2 tads that were successfully mapped. Coordinates are for species 1

'''



species1_tads, species2_tads_mapped, output_file = sys.argv[1:4]

species1_tads = pandas.read_table(species1_tads, header=None)
species2_tads_mapped = pandas.read_table(species2_tads_mapped, header=None)


#Make sure all species 2 mapped TADs belong to chromosomes in species 1 tads
chromosomes = list(set(numpy.array(species1_tads[0]).ravel().tolist()))
species2_tads_mapped = species2_tads_mapped[species2_tads_mapped[0].isin(chromosomes)]

#Make dict where key is chromosome id and entry is array of all speies 2 mapped tads to that chromosome
species2_tads_dict = {}
for i in chromosomes:
    species2_tads_dict[i] = numpy.array(species2_tads_mapped[species2_tads_mapped[0] == i])




species1_tads = numpy.array(species1_tads)


matches = 0
intersections = 0
neither = 0
match_array = []
intersect_array = []
neither_array = []

for i in species1_tads:
    chrom, start, stop, ID = i[0], i[1], i[2], i[3]
    approx = (float(stop-start)*0.10)
    start1 = start - approx
    start2 = start + approx
    stop1 = stop - approx
    stop2 = stop + approx
    species2_match = []
    species2_intersect = []
    
    for tad in species2_tads_dict[chrom]:
        tad = tad.tolist()
        if (start1 <= tad[1] <= start2) and (stop1 <= tad[2] <= stop2):
            species2_match.append(tad[3])
        elif not (tad[2] <= start or stop <= tad[1]):
            species2_intersect.append(tad[3])
        else:
            continue
            
    print str(ID)
    print "conserved matches = " + str(len(species2_match))
    print "intersections = " + str(len(species2_intersect)) + '\n'
    
    if len(species2_match) > 0:
        matches +=1
        entry = [ID]
        for x in species2_match:
            entry.append(x)
        match_array.append(entry)
    elif len(species2_intersect) > 0:
        intersections +=1
        entry = [ID]
        for x in species2_intersect:
            entry.append(str(int(x)))
        intersect_array.append(entry)
    else:
        neither +=1
        entry = [ID]
        neither_array.append(ID)
        
print "total conserved matches = " + str(matches) + '\n'
print "total intersecting tads = " + str(intersections) + '\n'
print "total nonmatching or nonintersecting tads = " + str(neither) + '\n'

match_array = pandas.DataFrame(match_array)
intersect_array = pandas.DataFrame(intersect_array)
neither_array = pandas.DataFrame(neither_array)

match_array.to_csv(output_file+'-TADmatches.txt', sep='\t', header=False, index=False)
intersect_array.to_csv(output_file+'-TADintersections.txt', sep='\t', header=False, index=False)
neither_array.to_csv(output_file+'-neither.txt', sep='\t', header=False, index=False)










