#!/usr/bin/env python

from matplotlib import pyplot as py
import numpy as np

N = 5
positions = np.arange(2, N)
width = 0.27

non_orth_values = [0.79, 0.56, 1.06]
orth_values = [1.10, 0.79, 1.10]

fig = py.figure()
subplot = fig.add_subplot(1,1,1)
subplot.set_xlabel('Groups of TADs')
subplot.set_ylabel('Percent Genes Mapped / Percent TADs')
subplot.set_xticks(positions+0.33)
subplot.set_xticklabels( ('Mapped, Overlap', 'Mapped No Overlap', 'Not Mapped') )
for label in subplot.get_xticklabels():
    label.set_fontname('Arial')
    label.set_fontsize(16)
a = subplot.bar(positions, non_orth_values, width, color ='r')
b = subplot.bar(positions+width, orth_values, width, color='b')
fig.legend([a,b], ['Non-Ortholog genes', 'Ortholog one2one genes'], loc=1, shadow=True, fancybox=True)
fig.savefig('mappedGenesBar.png')