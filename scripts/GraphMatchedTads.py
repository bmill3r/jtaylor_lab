#!/usr/bin/env python

from matplotlib import pyplot as py
import numpy as np


N = 5
positions = np.arange(N)
width = 0.27

#'total', 'mapped_total', 'neither', 'matches', 'intersections'
#human = [3492, 2657, 118, 421, 2118] #0.25 liftOver and 1.75 cutoff
#mouse = [3098, 2478, 339, 439, 1700] #0.25 liftOver and 1.75 cutoff

human = [7793, 5700, 179, 330, 5191] #0.25 liftOver and 1.0 cutoff
mouse = [6984, 5497, 717, 350, 4430] #0.25 liftOver and 1.0 cutoff

#human = [2881, 2210, 107, 376, 1727] #0.25 liftOver and 2.0 cutoff
#mouse = [2539, 2038, 286, 379, 1373] #0.25 liftOver and 2.0 cutoff

#human = [1413, 1098, 76, 157, 865] #0.25 liftOver and 3.0 cutoff
#mouse = [1241, 993, 156, 155, 682] #0.25 liftOver and 3.0 cutoff

fig = py.figure()
subplot = fig.add_subplot(1,1,1)
#subplot.set_xlabel('Groups of TADs')
subplot.set_ylabel('# of TADs')
subplot.set_xticks(positions+width)
subplot.set_xticklabels( ('Total TADs', 'Lifted TADs', 'Lifted No Overlap', 'Lifted Matched', 'Lifted Intersect') )
for label in subplot.get_xticklabels():
    label.set_fontname('Arial')
    label.set_fontsize(12)
a = subplot.bar(positions, human, width, color ='c')
b = subplot.bar(positions+width, mouse, width, color='m')
fig.legend([a,b], ['Human TADs', 'Mouse TADs'], loc=1, shadow=True, fancybox=True)

def autolabel(rects):
    # attach some text labels
    for rect in rects:
        height = rect.get_height()
        subplot.text(rect.get_x()+rect.get_width()/2., 1.05*height, '%d'%int(height),
                ha='center', va='bottom')

autolabel(a)
autolabel(b)

fig.savefig('MatchingTADS_cutoff1.png')