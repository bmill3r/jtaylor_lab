#!/usr/bin/python

import pandas
import numpy
import sys

'''
Takes peaks_output file and returns bed3 with coordinates 
of peaks to be used as track in UCSC genome browser
'''


peaks, output_name = sys.argv[1:3]

peaks = pandas.read_table(peaks, header=None)
peaks = numpy.array(peaks)

new_array = []
for i in peaks:
    chrom, coord1, coord2 = i[0], i[1], i[2]
    chrom = 'chr' + str(chrom)
    entry = (chrom, coord1, coord2)
    new_array.append(entry)

peaks = numpy.array(new_array)
peaks = pandas.DataFrame(peaks)

peaks.to_csv(output_name, sep='\t', header=False, index=False)