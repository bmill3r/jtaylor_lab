#!/usr/bin/env python

import pandas
import numpy
import sys


'''
Input file with list of features (in this case peaks that were mapped using bnMapper.py)
and input in BED4 file of all features (filtered, sorted, essentially the input used in bnMapper.py)
    ex: hESC_peak_intervals_bed4_filtered-20kb.txt

And output BED4 files of features, essentially one is features that mapped and other is features
that did not map. 
'''

mapped_feature_list, BED4_feature_file, output_name = sys.argv[1:4]

#Extract list of mapped features
mapped_feature_IDs = pandas.read_table(mapped_feature_list, header=None)
#mapped_feature_IDs = mapped_feature_IDs[0] #If inputting list
mapped_feature_IDs = mapped_feature_IDs[3] #if inputting BED4 files from liftOver...
mapped_feature_IDs = mapped_feature_IDs.tolist()


features = pandas.read_table(BED4_feature_file, header=None)
mapped_features = features[features[3].isin(mapped_feature_IDs)]
unmapped_features = features[-features[3].isin(mapped_feature_IDs)]

mapped_features.to_csv(output_name + '-mapped.txt', sep='\t', header=False, index=False)
unmapped_features.to_csv(output_name + '-unmapped.txt', sep='\t', header=False, index=False)




