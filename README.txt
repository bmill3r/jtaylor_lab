Data files are on HHPC.JHU



FILE TREE:

NOTE: Only used human chromosomes 1-20 and mouse chromosomes 1-19


GSE35156:

	Data -> *_data.dhf5 files
	HiFive -> *_hic.hdf5 files
	BI -> BI_out.hdf5 files

	Ensembl_tables -> files containing csv tables of human/mouse gene orthologs

	genome_chain_files -> chain files to lift features with liftOver

	UCSC_tables -> tracks and gene info from genome browser

	analysis -> initial TAD and peak files...filtered 1M, liftOver -multiple...etc...not used for project...see 'HESC_data' or 'mESC_HindIII_data'


DATA FOR PROJECT:
(liftOver -minMatch, and different cutoffs...)

	hESC_data -> cutoff 1.75, minMatch 0.25
	mESC_HindIII_data -> 1.75, minMatch 0.25
	
	*_test -> cutoff 1,2,3 minMatch 0.10, 0.25, 0.35

	genes -> from Ensembl-biomart...human/mouse orths and non-orths, genes in TADs

	
	for BED4 files....

		*_bed4.txt -> all TADs in specieis found with HiFive

		*-output.txt -> coordinates are to other species
				(if human, then coord for mouse...)

		*-mapped.txt -> coordinates are for own genome, and were successfully mapped to other species
				(if human, then coords for human)

		*-mapped-TADmatches.txt -> first column are TADs of species 1 (species in file name), other column is TADs from species 2 that mapped and matched one-to-one
 
		*-intersections and -neither also follow same pattern...first column are TADs located in species that is in file name



WORKFLOW:

for hESC and mESC_HindIII

./find_hic_BI

	generate BI.hdf5 files


./find_BI_peaks, with cutoffs (1, 1.75, 2, 3, etc)


./find_BI_and_peak_intervals
	generate bedGraphs, BED4s of all BI intervals with scores
	generate peak_intervals (BED3 with BI intervals that were BI peaks

./find_TADs
	generate BED3 of TADs using peak_intervals and BI_bedGraphs

./enumerate_BED3_to_BED4
	add IDs for each TAD

./liftOver...at minMatch=0.25

	hESC_TADs_cutoff1_025lift-output.txt
	mESC_HindIII_TADs_cutoff1_025lift-output.txt

./BED4-from-bnMap-list.py
	get mapped and unmapped TADs

	hESC_TADs_cutoff1_025lift-mapped.txt
	hESC_TADs_cutoff1_025lift-unmapped.txt


./mappedTADs_one2one_matches.py
	looks for intersections/one-to-one matches between TADs in species 1 that were able to be mapped to species 2 and species 2 TADs that were mapped to species 1

	returns number of TAD intersections and matches and those that did not match.


./GraphliftoverMappedFrequency.py

	plots number of TADs (in this case data is for TADs found with cutoff of 1.75) that are mapped for different values of minMatch threshold in liftOver

./GraphMatchedTads.py

	Graph total TADs, mapped TADs, matching TADs, intersecting TADs